#include <iostream>
#include <sqlite3.h>
#include <string>


int callback(void* NotUsed, int argc, char** argv, char** nzt) {
    // argc - liczba kolumn
    // argv - tablica wskaznikow na stringi
    // nzt - nazwy kolumn

    for (int i = 0; i < argc; i++) {
        // Show column name, value, and newline
        std::cout << nzt[i] << ": " << argv[i] << std::endl;

    }

    return 0;
}

int main() {
    sqlite3* DB;
    char* erW = 0;
    
    int op;
    op = sqlite3_open("baza.db", &DB);
    if (op) {
        std::cout << "Tabela error" << sqlite3_errmsg(DB) << std::endl;
        sqlite3_close(DB);
        return 1;
    }

    std::string table;
    table = "CREATE TABLE Uzy(ID_Klienta INTEGER PRIMARY KEY AUTOINCREMENT, "\
        "Login TEXT NOT NULL, "\
        "Stan_konta DECIMAL NOT NULL);";
     op = sqlite3_exec(DB, table.c_str(), callback, 0, &erW);

    
    
    std::string loginn;
    float kasa = 0;
    int wybrana = 0;
    int wplata = 0;
    int wyplata = 0;
    int Id_k = 0;
    int Id_p = 0;
    int kwp = 0;
    bool zalogowany = false;
    std::cout << "Witaj w banku" << std::endl;
    while (wybrana != 3)
    {
        std::cout << "\nwprowadz komende\n";
        if (zalogowany)
            std::cout << "wyjdz - 3     wplata - 4       wyplata -5      przelew - 6\n";
        else
            std::cout << "zaloz konto - 1        zaloguj - 2        wyjdz - 3\n";

        std::cin >> wybrana;
        switch (wybrana) {
        case 1:
            std::cout << "Podaj login: ";
            std::cin >> loginn;
            std::cout << "Podaj poczatkowy stan konta: ";
            std::cin >> kasa;
            table = "INSERT INTO Uzy ('Login', 'Stan_konta') VALUES ('" + loginn + "', '" + std::to_string(kasa) + "');";
            op = sqlite3_exec(DB, table.c_str(), callback, 0, &erW);
            break;
        case 2:
            zalogowany = true;
            std::cout << "Podaj login aby sie zalogowac" << std::endl;
            std::cin >> loginn;
            table = "SELECT * FROM Uzy WHERE Login = '" + loginn + "';";
            sqlite3_exec(DB, table.c_str(), callback, 0, &erW);
            std::cout << "\nAby dokonac wplaty wybierz 4, aby dokonac wyplaty wprowadz 5" << std::endl;
            std::cout << "\nAby wykonac przelew wybierz opcje 6\n" << std::endl;
            break;
        case 4:
            std::cout << "Podaj swoje ID_Klienta: ";
            std::cin >> Id_k;
            std::cout << "Podaj kwote wplaty: ";
            std::cin >> wplata;
            table = "UPDATE Uzy SET Stan_Konta = Stan_Konta +'" + std::to_string(wplata) + "' WHERE ID_klienta ='" + std::to_string(Id_k) + "';";
            sqlite3_exec(DB, table.c_str(), callback, 0, &erW);
            break;
        case 5:
            std::cout << "Podaj swoje ID_Klienta: ";
            std::cin >> Id_k;
            std::cout << "Podaj kwote wyplaty: ";
            std::cin >> wyplata;
            table = "UPDATE Uzy SET Stan_Konta = Stan_Konta -'" + std::to_string(wyplata) + "' WHERE ID_klienta ='" + std::to_string(Id_k) + "';";
            sqlite3_exec(DB, table.c_str(), callback, 0, &erW);
            break;
        case 6:
            std::cout << "Podaj swoje ID_Klienta: ";
            std::cin >> Id_k;
            std::cout << "Podaj ID_Klienta na ktore chcesz przelac pieniadze: ";
            std::cin >> Id_p;
            std::cout << "Podaj kwote przelewu: ";
            std::cin >> kwp;
            table = "UPDATE Uzy SET Stan_Konta = Stan_Konta -'" + std::to_string(kwp) + "' WHERE ID_klienta ='" + std::to_string(Id_k) + "';";
            sqlite3_exec(DB, table.c_str(), callback, 0, &erW);
            table = "UPDATE Uzy SET Stan_Konta = Stan_Konta +'" + std::to_string(kwp) + "' WHERE ID_klienta ='" + std::to_string(Id_p) + "';";
            sqlite3_exec(DB, table.c_str(), callback, 0, &erW);
            break;
        }
    }

    sqlite3_close(DB);
    return 0;
}
